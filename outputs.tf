output "acrfqdn" {
  value       = azurerm_container_registry.this.login_server
  description = "ACR login server"
}

output "acruser" {
  value       = azurerm_container_registry_token.this.name
  description = "ACR username"
}

output "acrpass" {
  value       = azurerm_container_registry_token_password.this.password1
  sensitive   = true
  description = "ACR password. Kann mittels terraform output -json angesehen werden"
}

output "acifqdn" {
  value       = azurerm_container_group.this.fqdn
  description = "ACI FQDN"
}
