

resource "azurerm_container_group" "this" {
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  name                = "abbtsclaaci${random_integer.random_suffix.result}"
  ip_address_type     = "Public"
  dns_name_label      = "abbtsclaaci${random_integer.random_suffix.result}"
  os_type             = "Linux"
  restart_policy      = "Never"
  image_registry_credential {
    username = azurerm_container_registry.this.admin_username
    password = azurerm_container_registry.this.admin_password
    server   = azurerm_container_registry.this.login_server
  }


  container {
    name   = "hello-world"
    image  = "${azurerm_container_registry.this.login_server}/apps/meineapp:latest"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 80
      protocol = "TCP"
    }
  }

}
