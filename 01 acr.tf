resource "azurerm_container_registry" "this" {
  name                = "abbtsclaacr${random_integer.random_suffix.result}"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  sku                 = "Basic"
  admin_enabled       = true
}

resource "azurerm_container_registry_scope_map" "this" {
  name                    = "meineapp-permissions"
  container_registry_name = azurerm_container_registry.this.name
  resource_group_name     = azurerm_resource_group.this.name
  actions = [
    "repositories/apps/meineapp/content/read",
    "repositories/apps/meineapp/content/write"
  ]
}

resource "azurerm_container_registry_token" "this" {
  name                    = "docker-local"
  container_registry_name = azurerm_container_registry.this.name
  resource_group_name     = azurerm_resource_group.this.name
  scope_map_id            = azurerm_container_registry_scope_map.this.id
}

resource "azurerm_container_registry_token_password" "this" {
  container_registry_token_id = azurerm_container_registry_token.this.id

  password1 {
  }
}
